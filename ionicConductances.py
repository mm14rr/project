#Ionic conductances test
#the change in the ionic conductances is given by
from math import exp as e     #for math.exp(x), the exponential function
import matplotlib.pyplot as plt
import numpy
import time

class Neuron():
    #A basic Hodgkin-Huxley neuron using parameters from Ca2+ model paper
    #tutorial at https://hodgkin-huxley-tutorial.readthedocs.io/en/latest/_static/Hodgkin%20Huxley.html
    #used to inform structure

    C_m = 1.
    #cell membrane capacitance in micro Farads/cm^2 (uF/cm^2)

    g_Na = 120.
    #sodium (Na) maximum conductance in the soma compartment in microSeimens/cm^2 (mS/cm^2)

    g_K = 100.
    #potassium (K) delayed rectifier in soma compartment maximum conductance (mS/cm^2)

    g_Ca_N_s = 14.
    #N like Calcium (Ca) maxium conductance in soma compartment (mS/cm^2)

    g_Ca_K_s = 5.
    #Ca dependant potassium maximum conductance in soma compartment (mS/cm^2)

    g_Ca_N_d = 0.3
    #N like Ca maximum conductance in dendrite compartment (mS/cm^2)

    g_Ca_K_d = 1.1
    #Ca dependant potassium maximum conductance in dendrite compartment (mS/cm^2)

    g_Ca_L = 0.33#0.33   #0.1 gives closer behaviour to Booth and Rinzel, but theres no basis for the change
    #L like Ca maximum conductance in dendrite compartment

    g_L = 0.51
    #leak maximum conductance in (mS/cm^2)

    g_c = 0.1
    #intracellular maximum conductance in (mS/cm^2)

    V_Na = 55.
    #reversal potential of sodium in mille volts (mV)

    V_K = -80.
    #reversal potential of potassium (mV)

    V_Ca = 80.
    #reversal potential of calcium (mV)

    V_L = -60.
    #reversal potential of the leak in mV
    #at rest the compartment tends toward the leak reversal potential, so start equal to it

    K_d = 0.2
    #half saturation of potassium for Ca2+ dependent K+ conductance in micro moles

    p = 0.1
    #the ration of soma area to total cell area

    timestep = 0.001
    t = numpy.arange(0.0, 1000, timestep)
    #time of which to intergrate over, using timestep/5 of the timestep used in the study to minimise error
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of sodium activation m

    def m_infinite(self, V):
        return 1./(1. + e((V - (-35.))/-7.8))
    #steady state function for sodium activation where theta_m = -35 and k_m = -7.8

    #at rest m tends to m_infinite(), so start it as equal
    ####functions concerning gating kinetics of sodium activation m
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of sodium inactivation h

    def t_h(self, V):
        return 30./(e((V + 50.)/15.) + e((-(V + 50.))/16.))
    #time constant for sodium inactivation

    def h_infinite(self, V):
        return 1./(1. + e((V - (-55.))/7.))
    #steady state function for sodium inactivation where theta_h = -55 and k_h = 7

    def dhdt(self, h, V):
        return (self.h_infinite(V) - h)/self.t_h(V)
    #gating kinetics of ionic conductance of sodium inactivation

    ####functions concerning gating kinetics of sodium inactivation h
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of potassium+ delayed rectifier activation n

    def t_n(self, V):
        return 7./(e((V + 40.)/40.) + e((-(V + 40.))/50.))
    #time constant for potassium plus delayed rectifier activation

    def n_infinite(self, V):
        return 1./(1. + e((V - (-28.))/-15.))
    #steady state function for potassium delayed rectifier activation where theta_n = -28 and k_n = -15

    def dndt(self, n, V):
        return (self.n_infinite(V) - n)/self.t_n(V)
    #gating kinetics of ionic conductance of potassium+ delayed rectifier activation

    ####functions concerning gating kinetics of potassium+ delayed rectifier n
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of Ca2+ activation mN

    def t_mN(self):
        return 4.
    #time constant for N-like Ca2+ activation

    def mN_infinite(self, V):
        return 1./(1. + e((V - (-30.))/-5.))
    #steady state function for N-Like Ca2+ activation where theta_mN = -30 and k_mN = -5

    def dmNdt(self, mN, V):
        return (self.mN_infinite(V) - mN)/self.t_mN()
    #gating kinetics of ionic conductance of Ca2+ activation

    ####functions concerning gating kinetics of N-Like Ca2+ activation mN
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of N-Like Ca2+ inactivation hN
    def t_hN(self):
        return 40.
    #time constant for N-like Ca2+ activation

    def hN_infinite(self, V):
        return 1./(1. + e((V - (-45.))/5.))
    #steady state function for N-Like Ca2+ inactivation where theta_hN = -45 and k_hN = 5

    def dhNdt(self, hN, V):
        return (self.hN_infinite(V) - hN)/self.t_hN()
    #gating kinetics of ionic conductance of Ca2+ inactivation

    ####functions concerning gating kinetics of N-Like Ca2+ inactivation hN
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions concerning gating kinetics of L-Like Ca2+ activation
    def t_mL(self):
        return 40.
    #time constant for L-like Ca2+ activation

    def mL_infinite(self, V):
        return 1./(1. + e((V - (-40.))/-7.))
    #steady state function for L-Like Ca2+ activation where theta_mL = -40 and k_mL = -7

    def dmLdt(self, mL, V):
        return (self.mL_infinite(V) - mL)/self.t_mL()
    #gating kinetics of ionic conductance of L-Like Ca2+ activation

    ####functions concerning gating kinetics of L-Like Ca2+ activation hN
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions for the intracellular concentrations of Ca2+

    def dCadt(self, I_Ca, Ca_s):
        return 0.01*((-0.0009)*I_Ca - 2*Ca_s)

    #Rate of change of the intracellular Ca2+ concentration in the soma, where:
    #f = 0.01 percent of free to bound Ca2+
    #CORRECTION: a = 0.0009 moles/C/micrometers coverts current to concentration
    #the value a = 0.009 given in the paper was incorrect, thanks to Hugh for noticing
    #k_Ca = 2 ms^-1 is the Ca2+ removal rate.

    ####functions for the intracellular concentrations of Ca2+
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ####functions of each ionic conductance
    #~~~ionic conductances in the soma

    def I_Na(self, V_s, m, h):
        return (-self.g_Na)*(m**3)*h*(V_s - self.V_Na)
    #Sodium conductance

    def I_K(self, V_s, n):
        return (-self.g_K)*(n**4)*(V_s - self.V_K)
    #Potassium delayed rectifier conductance

    def I_Ca_N_s(self, V_s, mN, hN):
        return (-self.g_Ca_N_s)*(mN**2)*(hN)*(V_s - self.V_Ca)
    #N-like calcium conductance

    def I_Ca_K_s(self, V_s, Ca_s):
        return (-self.g_Ca_K_s)*(Ca_s/(Ca_s+self.K_d))*(V_s - self.V_K)
    #Calcium dependant potassium conductance

    #I_Leak(self, V_s) implemented after dendrite compartment

    def I_intra_s(self, V_d, V_s):
        return (self.g_c/self.p)*(V_d - V_s)
    #intracellular conductance from the dendrite compartment

    #injection for control A
    def I_injA(self, t):
        return 6*(t>100) - 6*(t>900)
    #injecton for control B
    def I_injB(self, t):
        return 11*(t>100) - 11*(t>900)

    def I_injHH(self, t):
        return 6*(t>2) - 6*(t>8)

    #The injected current
    #~~~ionic conductances in the soma
    #~~~~~~
    #~~~ionic conductances in the dendrite
    def I_Ca_N_d(self, V_d, mN, hN):
        return (-self.g_Ca_N_d)*(mN**2)*(hN)*(V_d - self.V_Ca)
    #N-like calcium conductance

    def I_Ca_L_d(self, V_d, mL):
        return (-self.g_Ca_L)*mL*(V_d - self.V_Ca)
    #L-like calcium conductance

    def I_Ca_K_d(self, V_d, Ca_d):
        return (-self.g_Ca_K_d)*(Ca_d/(Ca_d+self.K_d))*(V_d - self.V_K)
    #Calcium dependant potassium conductance

    #I_Leak(self, V_d) implement after dendrite compartment

    def I_intra_d(self, V_s, V_d):
        return (self.g_c/(1 - self.p))*(V_s -V_d)
    #intracellular conductance from the soma compartment

    #~~~ionic conductances in the dendrite
    #share ionic conductances
    def I_Leak(self, V):
        return (-self.g_L)*(V - self.V_L)
    #shared leak conductance
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def Main(self):

        #take starting values for each variable

        #initialise the compartmental voltages
        V_s = self.V_L
        V_d = self.V_L
        #as at rest the systems will tend tend towards the leak reversal potential

        #initialise the compartmental calcium concentration
        #Ca_s = 0.
        #Ca_d = 0.
        #should be zero at rest as I_Ca will be zero and so Ca_x must be zero to keep dCaxdt at rest

        #initialise the gating variables for the soma compartment
        m_s = self.m_infinite(V_s)
        h_s = self.h_infinite(V_s)

        n_s = self.n_infinite(V_s)

        mN_s = self.mN_infinite(V_s)
        hN_s = self.hN_infinite(V_s)
        #at rest each gate is closed (0) so each gate will b the value of the infinite at rest

        #initialise the gating variables for the dendrite compartment
        mN_d = self.mN_infinite(V_d)
        hN_d = self.hN_infinite(V_d)

        mL_d = self.mL_infinite(V_d)
        #at rest each gate is closed (0) so each gate will b the value of the infinite at rest

        Ca_s =(-0.0009/2)*self.I_Ca_N_s(V_s, mN_s, hN_s)
        Ca_d =(-0.0009/2)*(self.I_Ca_N_d(V_d, mN_d, hN_d)+self.I_Ca_L_d(V_d, mL_d))
        #Ca_s =((-0.0009*0.01)/2)*self.I_Ca_N_s(V_s, mN_s, hN_s)
        #Ca_d =((-0.0009*0.01)/2)*(self.I_Ca_N_d(V_d, mN_d, hN_d)+self.I_Ca_L_d(V_d, mL_d))
        #initial Ca concentration is when dCa/dt = 0 so where -a*I_Ca/2 = Ca2+

        #stores the resulting data
        #soma components
        V_list_s = []

        m_list_s = []
        h_list_s = []
        n_list_s = []
        mN_list_s = []
        hN_list_s = []

        Ca_list_s = []


        I_Na_list_s = []
        I_K_list_s = []
        I_Ca_N_list_s = []
        I_Ca_K_list_s = []

        I_L_list_s = []

        I_intra_list_s = []

        I_inj_list_s = []

        #dendrite components
        V_list_d = []

        mN_list_d = []
        hN_list_d = []

        mL_list_d = []

        Ca_list_d = []

        I_Ca_N_list_d = []
        I_Ca_L_list_d = []
        I_Ca_K_list_d = []

        I_L_list_d = []

        I_intra_list_d = []

        #store the timer data to average later
        timer_list = []

        #read out the amount of steps in the integration
        print("Step length: " + str(self.timestep))
        print("Step number: " + str(len(self.t)))

        int_start = time.perf_counter()
        #manually integrate the system using euler's method
        for t_i in self.t:
            step_start = time.perf_counter()

            #if counter == 6737:
            #    for i in range(6737):
            #        testlist.append(self.t[i])
            #    self.t = testlist
            #    break

            #~~~~~~~~~~~~~~~~~~~~~~~~~~
            #first the soma compartment
            V_list_s.append(V_s)


            m_list_s.append(m_s)
            h_list_s.append(h_s)

            n_list_s.append(n_s)

            mN_list_s.append(mN_s)
            hN_list_s.append(hN_s)

            Ca_list_s.append(Ca_s)


            I_Na_list_s.append(self.I_Na(V_s, m_s, h_s))
            I_K_list_s.append(self.I_K(V_s, n_s))
            I_Ca_N_list_s.append(self.I_Ca_N_s(V_s, mN_s, hN_s))
            I_Ca_K_list_s.append(self.I_Ca_K_s(V_s, Ca_s))
            I_L_list_s.append(self.I_Leak(V_s))
            I_intra_list_s.append(self.I_intra_s(V_d, V_s))
            I_inj_list_s.append(self.I_injA(t_i))

            #now the dendrite compartment
            V_list_d.append(V_d)

            mN_list_d.append(mN_d)
            hN_list_d.append(hN_d)

            mL_list_d.append(mL_d)

            Ca_list_d.append(Ca_d)

            I_Ca_N_list_d.append(self.I_Ca_N_d(V_d, mN_d, hN_d))
            I_Ca_L_list_d.append(self.I_Ca_L_d(V_d, mL_d))

            I_Ca_K_list_d.append(self.I_Ca_K_d(V_d, Ca_d))

            I_L_list_d.append(self.I_Leak(V_d))

            I_intra_list_d.append(self.I_intra_d(V_s, V_d))


            #then intergrate using the current values with euler's method
            #store the next value for V_s seperately, as the current V_s is needed to find the other changes
            next_V_s = V_s + self.timestep*((self.I_Na(V_s, m_s, h_s) + self.I_K(V_s, n_s)+ self.I_Ca_N_s(V_s, mN_s, hN_s)
                + self.I_Ca_K_s(V_s, Ca_s) + self.I_Leak(V_s) + self.I_intra_s(V_d, V_s) +self.I_injA(t_i))/self.C_m)

            #now work out the new calcium concentration
            Ca_s = Ca_s + self.timestep*self.dCadt(self.I_Ca_N_s(V_s, mN_s, hN_s), Ca_s)

            #using the current V_s find the next gating variables

            m_s = self.m_infinite(V_s)


            h_s = h_s + self.timestep*self.dhdt(h_s, V_s)

            n_s = n_s + self.timestep*self.dndt(n_s, V_s)


            mN_s = mN_s + self.timestep*self.dmNdt(mN_s, V_s)

            hN_s = hN_s + self.timestep*self.dhNdt(hN_s, V_s)

            #~~~~~~~~~~~~~~~~~~~~~~~~~~
            #now the dendrite compartment
            #next_V_d = V_d + self.timestep*((self.I_Ca_N_d(V_d, mN_d, hN_d) #+ self.I_Ca_L_d(V_d, mL_d) + self.I_Ca_K_d(V_d, Ca_d)
            # + self.I_Leak(V_d) + self.I_intra_d(V_s, V_d))/self.C_m)
            next_V_d = V_d + self.timestep*( self.I_Ca_N_d(V_d, mN_d, hN_d) + self.I_Ca_L_d(V_d, mL_d)  + self.I_Ca_K_d(V_d, Ca_d) + self.I_Leak(V_d) + self.I_intra_d(V_s, V_d))

            Ca_d = Ca_d + self.timestep*self.dCadt(self.I_Ca_N_d(V_d, mN_d, hN_d)+ self.I_Ca_L_d(V_d, mL_d), Ca_d)

            #using the current V_d find the next gating variables
            mN_d = mN_d + self.timestep*self.dmNdt(mN_d, V_d)
            hN_d = hN_d + self.timestep*self.dhNdt(hN_d, V_d)

            mL_d = mL_d + self.timestep*self.dmLdt(mL_d, V_d)



            #set the next voltages as the current voltages
            V_s = next_V_s
            V_d = next_V_d
            #print("V_s: " + str(V_s) + "\nCa_s: "+ str(Ca_s))
            timer_list.append(time.perf_counter() - step_start)

        print("Average step time:" + str(numpy.mean(timer_list)))
        print("Total time:" + str(time.perf_counter() - int_start))




        plt.figure()
        #"""
        plt.subplot(4, 2, 1)
        plt.title('Soma Voltage')
        plt.plot(self.t, V_list_s, 'k', label = '$V_{s}$')
        plt.plot(self.t, V_list_d, 'r', label = '$V_{d}$')
        plt.plot(self.t, I_inj_list_s, 'y', label = '$I_{inj}$')
        plt.legend(loc = 1)
        plt.ylabel('Voltage (mV)')
        plt.xlabel('Time (ms)')


        #plt.subplot(3, 1, 3)
        #plt.plot(self.t, I_inj_list_s, 'k', label='$I_{inj}$')
        #plt.ylabel('Voltage (mV)')
        #plt.xlabel('Time (ms)')


        plt.subplot(4,2,2)
        plt.title('Dendrite Voltage')
        plt.plot(self.t, V_list_d, 'k')
        plt.ylabel('V (mV)')
        plt.xlabel('Time (ms)')

        plt.subplot(4,2,3)
        plt.title('Soma Ionic Conductances')
        plt.plot(self.t, I_Na_list_s, 'c', label='$I_{Na}$')
        plt.plot(self.t, I_K_list_s, 'k', label='$I_{K}$')
        plt.plot(self.t, I_Ca_N_list_s, 'r', label='$I_{Ca-N}$')
        plt.plot(self.t, I_Ca_K_list_s, 'g', label='$I_{K(Ca)}$')
        plt.plot(self.t, I_L_list_s, 'm', label='$I_{L}$')
        plt.plot(self.t, I_intra_list_s, 'b', label='$I_{intra}$')
        #plt.plot(self.t, I_inj_list_s, 'y', label='$I_{inj}$')
        plt.legend(loc = 1)
        plt.ylabel('Voltage (mV)')
        plt.xlabel('Time (ms)')
        plt.ylim(-250, 400)

        plt.subplot(4,2,4)
        plt.title('Dendrite Ionic Conductances')
        plt.plot(self.t, I_Ca_N_list_d, 'r', label='$I_{Ca-N}$')
        plt.plot(self.t, I_Ca_L_list_d, 'y', label='$I_{Ca-L}$')
        plt.plot(self.t, I_Ca_K_list_d, 'g', label='$I_{K(Ca)}$')
        plt.plot(self.t, I_L_list_d, 'm', label='$I_{L}$')
        plt.plot(self.t, I_intra_list_d, 'b', label='$I_{intra}$')
        plt.ylabel('Voltage (mV)')
        plt.xlabel('Time (ms)')
        plt.legend(loc = 1)

        plt.subplot(4,2,5)
        plt.title("Soma Gating Values")
        plt.plot(self.t, m_list_s, 'r', label='m')
        plt.plot(self.t, h_list_s, 'g', label='h')
        plt.plot(self.t, n_list_s, 'b', label='n')
        plt.plot(self.t, mN_list_s, 'c', label='mN')
        plt.plot(self.t, hN_list_s, 'k', label='hN')
        plt.legend(loc = 1)

        plt.subplot(4,2,6)
        plt.title("Dendrite Gating Values")
        plt.plot(self.t, mN_list_d, 'c', label='mN')
        plt.plot(self.t, hN_list_d, 'k', label='hN')
        plt.plot(self.t, mL_list_d, 'm', label='mL')
        plt.legend(loc = 1)

        plt.subplot(4,2,7)
        plt.title("Soma calcium concentration")
        plt.plot(self.t, Ca_list_s)

        plt.subplot(4,2,8)
        plt.title("Dendrite calcium concentration")
        plt.plot(self.t, Ca_list_d)

        
        plt.show()


runner = Neuron()
runner.Main()
