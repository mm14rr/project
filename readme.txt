readme.txt
-------------------
To run the 2-Compartmental Ca2+ Model using bash:
source venv/bin activate         #sets your activate python environment to the project's
python ionicConductances.py      #python script executing the model
